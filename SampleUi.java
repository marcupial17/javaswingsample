package epstraining;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

public class SampleUi extends Frame implements ActionListener {

	JTextField lastNameField;
	JTextField firstNameField;
	JComboBox genderDropDown;
	JTextField birthDateField;

	JButton addNewButton;
	JButton saveButton;
	JButton deleteButton;

	DefaultTableModel tableModel;
	JTable table;

	int id = 1;

	SampleUi() {
		JLabel lastName = new JLabel("Lastname:");
		lastName.setBounds(10, 5, 75, 10);

		lastNameField = new JTextField();
		lastNameField.setBounds(75, 5, 150, 20);
		lastNameField.setEnabled(false);

		JLabel firstName = new JLabel("Firstname:");
		firstName.setBounds(10, 30, 75, 10);

		firstNameField = new JTextField();
		firstNameField.setBounds(75, 30, 150, 20);
		firstNameField.setEnabled(false);

		JLabel gender = new JLabel("Gender:");
		gender.setBounds(10, 55, 75, 10);

		String genderArray[] = { "Male", "Female" };
		genderDropDown = new JComboBox(genderArray);
		genderDropDown.setBounds(75, 55, 150, 20);
		genderDropDown.setEnabled(false);

		JLabel birthDate = new JLabel("Birthdate:");
		birthDate.setBounds(10, 80, 75, 10);

		birthDateField = new JTextField();
		birthDateField.setBounds(75, 80, 150, 20);
		birthDateField.setEnabled(false);

		addNewButton = new JButton("Add New");
		addNewButton.setBounds(10, 105, 90, 20);
		addNewButton.setBackground(Color.GREEN);
		addNewButton.setForeground(Color.WHITE);

		saveButton = new JButton("Save");
		saveButton.setBounds(105, 105, 90, 20);
		saveButton.setBackground(Color.BLUE);
		saveButton.setForeground(Color.WHITE);
		saveButton.setEnabled(false);

		deleteButton = new JButton("Delete");
		deleteButton.setBounds(200, 105, 90, 20);
		deleteButton.setBackground(Color.RED);
		deleteButton.setForeground(Color.WHITE);
		deleteButton.setEnabled(false);

		String columnNames[] = { "ID", "Lastname", "Firstname", "Gender", "Birthdate" };
		tableModel = new DefaultTableModel(columnNames, 0);
		table = new JTable(tableModel);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setMinimumSize(new Dimension(480, 200));
		scrollPane.setPreferredSize(new Dimension(480, 200));

		addNewButton.addActionListener(this);
		saveButton.addActionListener(this);
		deleteButton.addActionListener(this);

		JPanel formPanel = new JPanel();
		formPanel.setBounds(10, 5, 500, 150);

		JPanel tablePanel = new JPanel();
		tablePanel.setBounds(5, 160, 480, 200);

		formPanel.add(lastName);
		formPanel.add(lastNameField);
		formPanel.add(firstName);
		formPanel.add(firstNameField);
		formPanel.add(gender);
		formPanel.add(genderDropDown);
		formPanel.add(birthDate);
		formPanel.add(birthDateField);
		formPanel.add(addNewButton);
		formPanel.add(saveButton);
		formPanel.add(deleteButton);
		formPanel.setLayout(null);

		tablePanel.add(scrollPane);

		JFrame frame = new JFrame();
		frame.setLayout(null);
		frame.add(formPanel);
		frame.add(tablePanel);

		frame.setSize(500, 420);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == addNewButton) {
			saveButton.setEnabled(true);
			lastNameField.setEnabled(true);
			firstNameField.setEnabled(true);
			genderDropDown.setEnabled(true);
			birthDateField.setEnabled(true);
		} else if (e.getSource() == saveButton) {
			tableModel.addRow(new Object[] { Integer.toString(id), lastNameField.getText(), firstNameField.getText(),
					genderDropDown.getSelectedItem().toString(), birthDateField.getText() });
			id++;
			lastNameField.setText("");
			firstNameField.setText("");
			genderDropDown.setSelectedItem("");
			birthDateField.setText("");
			
			saveButton.setEnabled(false);
			lastNameField.setEnabled(false);
			firstNameField.setEnabled(false);
			genderDropDown.setEnabled(false);
			birthDateField.setEnabled(false);
		} else if (e.getSource() == deleteButton) {
			tableModel.removeRow(table.getSelectedRow());
			deleteButton.setEnabled(false);
		} 
			
		table.addMouseListener(new java.awt.event.MouseAdapter() {
				@Override
				 public void mouseClicked(java.awt.event.MouseEvent evt) {
					deleteButton.setEnabled(true);
				}
		});
	}

	public static void main(String[] args) {
		new SampleUi();
	}

}

